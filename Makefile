#
# @file     Makefile
# @brief    Makefile for readline
#
# @note     $ sudo apt-get install libreadline6 libreadline6-dev
#
# @date     2017-03-19
# @authr    ymzk
#

PRJNAME =   readline

TARGET  = $(PRJNAME)
SRCS    = $(TARGET:%=%.c)
OBJS    = $(SRCS:%.c=%.o)
DEPEND  =   Makefile.depend

CC      = $(CROSS_COMPILE)gcc
INC     =  -I.
CFLAGS  = $(INC) -c -Wall -Werror
LDFLAGS = 
LIBS    = -lreadline

.PHONY: all
all: $(TARGET)

.PHONY: $(TARGET)
$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $< $(LIBS)

.c.o:
	$(CC) $(CFLAGS) $<

.PHONY: clean
clean:
	-rm -rf $(OBJS) $(TARGET) *~

.PHONY: depend
depend:
	-rm -rf $(DEPEND)
	$(CC) -MM -MG $(CFLAGS) $(SRCS) > $(DEPEND)

-include  $(DEPEND)

# end

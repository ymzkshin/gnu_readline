/**
 * @file     readline.c
 * @brief    test app for GNU readline library
 *
 * @note     see: ** GNU History Library
 *                https://cnswww.cns.cwru.edu/php/chet/readline/history.html
 *
 * @date     2017-03-19
 * @author   ymzk
 */

#include <stdio.h>              /* printf()      */
#include <string.h>             /* strcmp()      */
#include <stdlib.h>             /* free()        */

#include <readline/readline.h>  /* readlien()    */
#include <readline/history.h>   /* add_history() */

/**
 * _load_history()
 */
static void
_load_history(char *filehist)
{
    int          stat;
    HIST_ENTRY **history = NULL;

    stat = read_history(filehist);

    if (stat == 0) {
        int  idx = 0;

        printf(" load a history file successfully.: %s\n", filehist);
        history = history_list();

        while (history[idx] != NULL) {
            printf(" %4d: %s\n", idx, history[idx]->line);
            idx++;
        }
    }
}

/**
 * _save_history()
 */
static void
_save_history(char *filehist)
{
    if (history_length > 0) {   /* stored at least one entry ? */
        if (write_history(filehist) == 0) {
            printf(" save a history file successfully.: %s\n", filehist);
        }
    }
}

/* 
 */
static void
_clear_history(char *filehist)
{
    clear_history();
    remove(filehist);
}

/**
 * _show_usage()
 */
static void
_show_usage(void)
{
    printf("\n");
    printf("Usage:\n");
    printf(" q    : quit\n");
    printf(" ?    : this help\n");
    printf(" clear: clear the hsitory\n");
    printf("\n");
    printf(" C-p: revious-history\n");
    printf(" C-n: next-history\n");
    printf(" C-r: reverse-search-history\n");
    printf(" C-s: forward-search-history\n");
    printf(" See man bash for more details\n");
}

/**
 * main()
 * @brief    main routine for mq tx
 *
 * @param    [in]   argc        int ::= # of args
 * @param    [in]  *argv[]     char ::= entity of args
 * @return          stat            ::= process termination (EXIT_)
 */
int
main(int argc, char *argv[])
{
    char *buf;
    char *filehist = "./.histroy";

    printf(" Type ? for help\n");

    /* 1. load the history list from a file, */
    _load_history(filehist);

    /* 2. history test */
    while((buf = readline("> ")) != NULL) {
        if        (strcmp(buf, "q"    ) == 0) { /* quit  ? */
            break;
        } else if (strcmp(buf, "?"    ) == 0) { /* help  ? */
            _show_usage();
            continue;
        } else if (strcmp(buf, "clear") == 0) { /* clear ?*/
            _clear_history(filehist);
            continue;
        }

        if (buf[0] != '\0') {
            printf(" inout[%d]: %s\n", history_length, buf);
            add_history(buf);
        }
    }
    free(buf);                  /* free the memory space */

    /* 3. save the history list to   a file */
    _save_history(filehist);

    return  EXIT_SUCCESS;
}

/* end */
